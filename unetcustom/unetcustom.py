from tf_unet import unet, image_util

from pathlib import Path
from typing import List

from ccdetectorinterface import CCDetector, RgbImg, XYVertices
from prochelpers import populateTrainingFiles

class FeatureImgDataProvider(image_util.ImageDataProvider):

  def _next_data(self):
    img, label =  super()._next_data()
    featureImg = img


    return featureImg, label


def createModel(trainFolder: Path):
  files = str(trainFolder/'*.png')
  data_provider = image_util.ImageDataProvider(files, data_suffix='.png',
                                               mask_suffix='_mask.png')

  net = unet.Unet(layers=5, features_root=128, channels=1, n_class=2)
  trainer = unet.Trainer(net)
  path = trainer.train(data_provider, './Checkpoints', training_iters=15, epochs=1000)

class UNetCCDetect(CCDetector):
  def __init__(self):
    super().__init__('UNet')



  def _findCCImpl(self, ccImg: RgbImg) -> List[XYVertices]:
    pass


if __name__ == '__main__':
  base = Path('../RepresentativeImgs/GroundTruth')
  paths = [Path(stri) for stri in (base/'CC', base/'NoCC')]
  populateTrainingFiles(*paths, base / 'NNData')
  createModel(base / 'NNData')
