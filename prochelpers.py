from ast import literal_eval
from pathlib import Path
from typing import Dict
from typing import List

import cv2 as cv
import numpy as np
import pyqtgraph as pg
from pandas import DataFrame as df
from scipy.ndimage import binary_fill_holes
from skimage import io
from skimage import transform
from skimage.color import label2rgb
from skimage.measure import label, regionprops
from skimage.util import montage
from tqdm import tqdm

from ccdetectorinterface import BlackWhiteImg, CCDetector, RgbImg, RgbaImg
from disputils import ImageCompositor

pg.setConfigOption('imageAxisOrder', 'row-major')

def _showHideWin(win):
  win.show()
  win.hide()

def getMaskFromCC_noCCDiff(ccImg: RgbImg, noCCImg: RgbImg) -> (BlackWhiteImg, np.ndarray):
  ccImgShape = ccImg.shape[:2]
  noCCImg = noCCImg[0:ccImgShape[0], 0:ccImgShape[1], :]
  diffImg = np.any((ccImg - noCCImg) > 0, 2)
  filledDiffImg = binary_fill_holes(diffImg)
  # Ensure only largest object is considered ground truth
  regions = regionprops(label(filledDiffImg))
  regionSizes = [r.bbox_area for r in regions]
  maxRegion = regions[int(np.argmax(regionSizes))]
  bbox = maxRegion.bbox
  # coords = maxRegion.coords
  filledDiffImg.fill(False)
  filledDiffImg[bbox[0]:bbox[2], bbox[1]:bbox[3]] = True
  return filledDiffImg, bbox

def genGroundTruth(ccImgFolder: str, noCCImgFolder: str, outFile: str):
  Path(outFile).parent.mkdir(parents=True, exist_ok=True)
  imFiles = [list(Path(folder).glob('*.png')) for folder in (ccImgFolder, noCCImgFolder)]
  processedFnames = []
  outArr = []
  for files in tqdm(zip(*imFiles)):
    curRow = []
    ccImg, noCCImg = [io.imread(curfile) for curfile in files]
    ccImgShape = ccImg.shape[:2]
    curRow.append(ccImgShape)
    _, coords = getMaskFromCC_noCCDiff(ccImg, noCCImg)
    ccBounds = [
      [coords[1], coords[0]],
      [coords[3], coords[0]],
      [coords[3], coords[2]],
      [coords[1], coords[2]]
    ]
    curRow.append(ccBounds)
    outArr.append(curRow)
    processedFnames.append(files[0].name)
    outDf = df(outArr, index=processedFnames)
    outDf.to_csv(outFile)

def mkImgToGTruthMapping(gTruthDf: df):
  mapping: Dict[str, BlackWhiteImg] = {}
  for idx, row in gTruthDf.iterrows():
    coords = literal_eval(row['1'])
    coords = np.array(coords)
    imgShape = literal_eval(row['0'])
    gTruthImg = np.zeros(imgShape, dtype='uint8')
    cv.fillPoly(gTruthImg, [coords], 1)
    mapping[idx.rsplit('_',2)[0]] = gTruthImg.astype(bool)
  return mapping

def overlayDetections(procs: List[CCDetector], ccImg: RgbImg, alphas: float,
                      groundTruth: BlackWhiteImg=None, colors: list=None,
                      saveFile:str=None) -> RgbaImg:
  masks = []
  names = []
  if groundTruth is not None:
    masks.append(groundTruth)
    names.append('Ground Truth')
  for proc in procs:
    proc.findCC(ccImg)
    mask = proc.lastCCToImg(ccImg.shape).astype('uint8')
    masks.append(mask)
    names.append(proc.name)
  compositor = ImageCompositor()
  compositor.setBaseImg(ccImg)
  compositor.addMasks(masks, None, alphas, colors)
  if saveFile is not None:
    compositor.save(saveFile)
  return compositor.toNumpy()

def getImg_maskFiles(folder: Path):
  imgs, masks = [], []
  for curFile in folder.glob('*.png'):
    if str(curFile).endswith('_mask.png'):
      masks.append(curFile)
    else:
      imgs.append(curFile)
  return sorted(imgs), sorted(masks)

def tileOverlays():
  base = Path('.')
  p1 = sorted(list(base.glob('*[!mask].png')))
  p2 = sorted(list(base.glob('*mask.png')))
  outFolder = base / 'tiled'
  outFolder.mkdir(exist_ok=True)

  allImgs = []
  firstImg = lastImg = ''
  miniPerImg = 25
  ii = 1
  for f1, f2 in tqdm(zip(p1, p2)):
    if (ii % (miniPerImg + 1)) == 1:
      firstImg = f1.stem
    x = io.imread(str(f1))
    m = io.imread(str(f2))
    out = label2rgb(m, x)
    if (ii % (miniPerImg + 1)) == 0:
      lastImg = f1.stem
      arr = np.stack(allImgs, axis=0)
      out = montage(arr, multichannel=True)
      io.imsave(str(outFolder / f'{firstImg}-{lastImg}.png'), out)
      allImgs = []
    allImgs.append(out)
    ii += 1



def populateTrainingFiles(ccImgFolder: Path, noCCImgFolder: Path, outFolder: Path):
  outFolder.mkdir(parents=True, exist_ok=True)
  imFiles = [Path(folder).glob('*.png') for folder in (ccImgFolder, noCCImgFolder)]
  # compositor = ImageCompositor()
  for ccFile, noCCFile in tqdm(list(zip(*imFiles))):
    imgs = [io.imread(curfile) for curfile in (ccFile, noCCFile)]
    for ii, img in enumerate(imgs):
      if img.shape[1] < img.shape[0]:
        img = np.rot90(img)
      img = (transform.resize(img, (400,600))*255).astype('uint8')
      imgs[ii] = img
    ccImg, noCCImg = imgs

    mask, _ = getMaskFromCC_noCCDiff(ccImg, noCCImg)

    # compositor.setBaseImg(ccImg)
    # compositor.addMasks(([mask]))
    # compositor.show()
    # compositor.clearOverlays()

    io.imsave(str(outFolder/f'{ccFile.stem}_mask.png'), mask.astype('uint8'))
    io.imsave(str(outFolder/ccFile.name), ccImg)


if __name__ == '__main__':
  baseFolder = Path('/home/UFAD/njessurun/Downloads/')
  ccFolder = baseFolder / 'Set1_ground_truth_extracted'
  noCCFolder = baseFolder / 'Set1_ground_truth_NoCC_extracted'
  # baseFolder = Path('./RepresentativeImgs/Test')
  # genGroundTruth(baseFolder/'CC', baseFolder/'NoCC', baseFolder/'bounds.csv')
  outFolder = Path('./tmp/')
  outFolder.mkdir(exist_ok=True)
  populateTrainingFiles(ccFolder, noCCFolder, outFolder)
elif False:
  mapping = mkImgToGTruthMapping(pd.read_csv('./RepresentativeImgs/GroundTruth/bounds.csv',
                                             index_col=0))
  for curFile in tqdm(list(baseFolder.glob('*.png'))):
    im = io.imread(str(curFile))
    gTruthMap = curFile.name.rsplit('_',2)[0]
    outArr = overlayDetections([Macduff()], im, 0.5, mapping[gTruthMap],
                               saveFile=str(outFolder/curFile.name))
