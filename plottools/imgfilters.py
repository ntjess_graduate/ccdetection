from skimage.util import montage
import matplotlib.pyplot as plt
from skimage import io

import pandas as pd
from pandas import DataFrame as df
import numpy as np

iou = pd.read_csv('./iou_fullData.csv', index_col=0)
coords = pd.read_csv('./coords_fullData.csv', index_col=0)

iouVals, coordVals = [curDf.values for curDf in (iou, coords)]
upper, lower = 0.7, 0.2
hasCoords = coordVals != '[]'

imgFileLists = []
namePerList  = []

# Good
idxs = np.all(iouVals > upper, 1)
imgFileLists.append(iou.index[idxs])
namePerList.append(f'All > {upper} IOU')


# Bad
idxs = (np.all(iouVals < lower, 1))
imgFileLists.append(iou.index[idxs])
namePerList.append(f'All < {lower} IOU')

imgList = []
forPrintImgs = []
allCheckers = np.arange(3, dtype=int)
checkerNames = iou.columns.values
for curChecker in 0, 1, 2:
  otherIdxs = np.setdiff1d(allCheckers, curChecker)
  onePass = ((np.all(iouVals[:,otherIdxs] < lower, 1))
             & (iouVals[:, curChecker] > upper))
  onePass_print = onePass & np.all(hasCoords[:, otherIdxs], 1)
  if onePass_print.sum() == 0:
    onePass_print = onePass & np.any(hasCoords[:, otherIdxs], 1)
  if onePass_print.sum() == 0:
    onePass_print = onePass
  forPrintImgs.append(iou.index[onePass_print])

  oneFailed = ((iouVals[:, curChecker] < lower)
                & (np.all(iouVals[:,otherIdxs] > upper, 1)))
  oneFailed_print = oneFailed & np.all(hasCoords[:, otherIdxs], 1)
  if oneFailed_print.sum() == 0:
    oneFailed_print = oneFailed & np.any(hasCoords[:, otherIdxs], 1)
  if oneFailed_print.sum() == 0:
    oneFailed_print = oneFailed
  forPrintImgs.append(iou.index[oneFailed_print])
  imgFileLists.append(iou.index[onePass])
  namePerList.append(f'Only {checkerNames[curChecker]} > {upper} IOU')
  imgFileLists.append(iou.index[oneFailed])
  namePerList.append(f'Only {checkerNames[curChecker]} < {lower} IOU')

# for imgFiles, name in zip(imgFileLists, namePerList):
#   print(f'{name}:\t{len(imgFiles)}')
numImgsPerLst = [len(lst) for lst in imgFileLists]
out = df([namePerList, numImgsPerLst]).T
outDf = out.set_axis(['Criteria', '# Images'], axis=1)
print(outDf)

# for curName, lst in zip(namePerList, imgFileLists[:2] + forPrintImgs):
#   toConsider = lst[:9]
#   imgs = [io.imread(f'Overlays/{imgName}') for imgName in toConsider]
#   img = np.stack(imgs, axis=0)
#   outImgs = montage(img, multichannel=True)
#   dispNames = np.array(toConsider)
#   try:
#     dispNames = dispNames.reshape(3,3)
#   except ValueError:
#     pass
#   print(df(dispNames))
#   plt.figure()
#   plt.imshow(outImgs)
#   plt.title(curName)
#   plt.show()

# idxs = (np.all(iouVals < lower, 1))
# condtn = hasCoords.sum(1) >= 2
# badList = iou.index[condtn & idxs]
#
#
# while True:
#    idxs = np.random.permutation(len(badList))
#    toConsider = badList[idxs[:9]]
#    imgs = [io.imread(f'Overlays/{imgName}') for imgName in toConsider]
#    img = np.stack(imgs, axis=0)
#    outImgs = montage(img, multichannel=True)
#    dispNames = np.array(toConsider)
#    try:
#      dispNames = dispNames.reshape(3,3)
#    except ValueError:
#      pass
#    print(df(dispNames))
#    plt.figure()
#    plt.imshow(outImgs)
#    plt.show()